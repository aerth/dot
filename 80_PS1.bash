# # fixed by aerth 2018-09-08
# #  this file depends on the other file in the directory, 
# #  for the __git_ps1 var
# #

# #  Customize BASH PS1 prompt to show current GIT repository and branch.
# #  by Mike Stewart - http://MediaDoneRight.com

# #  SETUP CONSTANTS
# #  Bunch-o-predefined colors.  Makes reading code easier than escape sequences.
# #  I don't remember where I found this.  o_O

# # Reset
# Color_Off="\[\033[0m\]"       # Text Reset

# # Regular Colors
# Black="\[\033[0;30m\]"        # Black
# Red="\[\033[0;31m\]"          # Red
# Green="\[\033[0;32m\]"        # Green
# Yellow="\[\033[0;33m\]"       # Yellow
# Blue="\[\033[0;34m\]"         # Blue
# Purple="\[\033[0;35m\]"       # Purple
# Cyan="\[\033[0;36m\]"         # Cyan
# White="\[\033[0;37m\]"        # White

# # Bold
# BBlack="\[\033[1;30m\]"       # Black
# BRed="\[\033[1;31m\]"         # Red
# BGreen="\[\033[1;32m\]"       # Green
# BYellow="\[\033[1;33m\]"      # Yellow
# BBlue="\[\033[1;34m\]"        # Blue
# BPurple="\[\033[1;35m\]"      # Purple
# BCyan="\[\033[1;36m\]"        # Cyan
# BWhite="\[\033[1;37m\]"       # White

# # Underline
# UBlack="\[\033[4;30m\]"       # Black
# URed="\[\033[4;31m\]"         # Red
# UGreen="\[\033[4;32m\]"       # Green
# UYellow="\[\033[4;33m\]"      # Yellow
# UBlue="\[\033[4;34m\]"        # Blue
# UPurple="\[\033[4;35m\]"      # Purple
# UCyan="\[\033[4;36m\]"        # Cyan
# UWhite="\[\033[4;37m\]"       # White

# # Background
# On_Black="\[\033[40m\]"       # Black
# On_Red="\[\033[41m\]"         # Red
# On_Green="\[\033[42m\]"       # Green
# On_Yellow="\[\033[43m\]"      # Yellow
# On_Blue="\[\033[44m\]"        # Blue
# On_Purple="\[\033[45m\]"      # Purple
# On_Cyan="\[\033[46m\]"        # Cyan
# On_White="\[\033[47m\]"       # White

# # High Intensty
# IBlack="\[\033[0;90m\]"       # Black
# IRed="\[\033[0;91m\]"         # Red
# IGreen="\[\033[0;92m\]"       # Green
# IYellow="\[\033[0;93m\]"      # Yellow
# IBlue="\[\033[0;94m\]"        # Blue
# IPurple="\[\033[0;95m\]"      # Purple
# ICyan="\[\033[0;96m\]"        # Cyan
# IWhite="\[\033[0;97m\]"       # White

# # Bold High Intensty
# BIBlack="\[\033[1;90m\]"      # Black
# BIRed="\[\033[1;91m\]"        # Red
# BIGreen="\[\033[1;92m\]"      # Green
# BIYellow="\[\033[1;93m\]"     # Yellow
# BIBlue="\[\033[1;94m\]"       # Blue
# BIPurple="\[\033[1;95m\]"     # Purple
# BICyan="\[\033[1;96m\]"       # Cyan
# BIWhite="\[\033[1;97m\]"      # White

# # High Intensty backgrounds
# On_IBlack="\[\033[0;100m\]"   # Black
# On_IRed="\[\033[0;101m\]"     # Red
# On_IGreen="\[\033[0;102m\]"   # Green
# On_IYellow="\[\033[0;103m\]"  # Yellow
# On_IBlue="\[\033[0;104m\]"    # Blue
# On_IPurple="\[\033[10;95m\]"  # Purple
# On_ICyan="\[\033[0;106m\]"    # Cyan
# On_IWhite="\[\033[0;107m\]"   # White
# dops1(){
#     export TZ=UTC
#     # Various variables you might want for your PS1 prompt instead
#     Time12h="\T"
#     Time12a="\@"
#     PathShort="\w"
#     PathFull="\W"
#     NewLine="\n"
#     Jobs="\j"

#     egrepline=/source\|/src

#     # somehow adapted from http://allancraig.net/index.php?option=com_content&view=article&id=108:ps1-export-command-for-git&catid=45:general&Itemid=96
#     #export PS1=$ICyan$Time12h$Color_Off
#     PS001="$Yellow$PathShort$Color_Off"
#     getit(){
#     num_branches=$(git branch 2>/dev/null)
#     echo last $?
#     if [ "0" -ne "$?" ]; then 
#       echo "nope"
#     fi
#     if [ -n "$num_branches" ]; then
#         # discover color
#         gitstatus=$(git status 2>/dev/null | grep 'nothing to commit')
#         echo gitstatus $gitstatus
#         # string
#         if [ -n "$gitstatus" ]; then
#           gitmsg=$(__git_ps1 "(%s)")
#           echo "${Green}${gitmsg}${Color_Off}"
#           exit 0;
#         fi
#         gitmsg=$(__git_ps1 "{%s}")
#         echo "${Red}${gitmsg}${Color_Off}"
#         exit 0
#     fi
#     # echo "${PS001} \$ "
#     exit 0;
#     }
#     # echo $(getit)
#     export PS1="$(getit) ${PS001} \$ "
# }
# dops1

# export GIT_PS1_SHOWCOLORHINTS=1
# PS1='\u@\h \W$(__git_ps1 " (%s)") \$ '
# PS1='\w \$ '
# PROMPT_COMMAND=__git_ps1
# PROMPT_COMMAND='__git_ps1 "\u@\h:\w" " \\$ "' 
PROMPT_COMMAND='__git_ps1 "\w" " \\$ "' 

# somehow adapted from http://allancraig.net/index.php?option=com_content&view=article&id=108:ps1-export-command-for-git&catid=45:general&Itemid=96
#export PS1=$ICyan$Time12h$Color_Off
# getit(){
# # normal operation
# export PS1="$Yellow$PathShort$Color_Off \$"
# if [ -z "$(pwd | egrep $egrepline)" ]; then \
#   export PS1="$Yellow$PathShort$Color_Off \$"
# else if [ -z $(pwd | grep -v /.git) ]; then
#   git branch &>/dev/null;
#   # other normal operation :)
#   if [ $? -eq 0 ]; then 
#     echo "$(echo `git status` | grep "nothing to commit" > /dev/null 2>&1; \
#     if [ "$?" -eq "0" ]; then \
#       # @4 - Clean repository - nothing to commit
#       echo "$Green"$(__git_ps1 "(%s)"); \
#     else \
#       # @5 - Changes to working tree
#       echo "$IRed"$(__git_ps1 "{%s}"); \
#     fi) $BYellow$PathShort$Color_Off \$ "; \
#   not git repo
#   else echo "$Yellow$PathShort$Color_Off \$ "; fi; 
#   else
#    export PS1="$Yellow$PathShort$Color_Off \$"

#   fi
# fi
# # }
# export PS1=$(getit)
# }
# dops1
