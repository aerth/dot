# dots done right

This is a revolutionary organization system for workspace configuration.

## initial setup (quick)
Run this to install:
```
curl https://gitlab.com/aerth/dot/-/raw/master/auto_install | bash
```

You can always customize the ~/.local/dot/ files later.

After customizing, just run `. ~/.bashrc` again

## manual setup and customize

clone this repo:

```
git clone https://gitlab.com/aerth/dot.git $HOME/.local/dot
git checkout -b $USER # your branch, customize it.
./install_dots # safe to run multiple times

```

it adds this to your ~/.bashrc:

```
for i in $(ls ~/.local/dot/*.bash); do source $i; done
```

or, use that line when you need it, to activate only sometimes..

now add things, such as the two provided (using numbers for ordering)

Copyright aerth  <aerth@riseup.net> 2018

Free software, MIT

modify heavily

## WHY DOES THIS EXIST?

  * one should not have to think much when switching workspaces (vms, servers, new pc, whatever)
  * simply keep track of your ~/.local directory, its one of the best.

