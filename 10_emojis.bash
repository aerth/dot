# declares an array with the emojis we want to support
# orig source: https://loige.co/random-emoji-in-your-prompt-how-and-why/
EMOJIS=( 😀 😃 😄 😁 😆 😅 😂 ☺️ 😊 😇 🙃 😉 😌 😍 😘 😗 😙 😚 😋 😜 😝 😛 😎 😏 😒 😞 😔 😟 😕 ☹️  😣 😖 😫 😩 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 😭 😓 😪 😴 😷 😈 ☠️  😺 😸 😹 😻 😼 😽 🙀 😿 😾 ✌️ ☝️ ✍️ )
rand_emoji(){
  # selects a random element from the EMOJIS set
  if [ "${#EMOJIS[@]}" == "0" ]; then
    echo "😀"
  else
  echo ${EMOJIS[$RANDOM % ${#EMOJIS[@]}]};
  fi
}
PS1='$(rand_emoji) '"$PS1"

